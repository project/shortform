 
Create short form's from any form, mostly to place in a block.

It does this mostly through jquery:

  - removing the field title, and if the field has no value, and when the
    field does not have the focus, displays the field's title as it's value.
    the title in the INPUT value field.
  - moving the field description from a div after the field into the HTML
     field element's description , which generally displays as mouse-over
     text.
  - removing fieldsets
  - removing non-required textarea's

Short Form can be used by module developer's:

  - $form['#theme'] = 'shortform';
  - shortform_alter($form);

Short Form can be used by site builder's:

  - on admin/settings/shortform, enable the "spy"
  - goto the page with the form you'd like to use
  - on admin/settings/shortform, check the form you'd like to use
  - on admin/build/block, add the block to your website
  - remember to configure the block's visibility

TODO
  - block configuration options giving the site builder greater flexibility
    on what to display
